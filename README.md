# What is NoSql?

NoSQL databases (aka "not only SQL") are non tabular, and store data differently than relational tables. NoSQL databases come in a variety of types based on their data model. The main types are document, key-value, wide-column, and graph.


# Why use NoSql databases?


 1. NoSql allows to create flexible schemas and scale easily with large amounts of data and high user loads. They are basically good for large and unstructured data.


 2. As storage costs rapidly decreased, the amount of data applications needed to store and query increased. This data came in all shapes and sizes—structured, semistructured, and polymorphic—and defining the schema in advance became nearly impossible. NoSQL databases allow developers to store huge amounts of unstructured data, giving them a lot of flexibility.

 3. NoSQL databases require much less hands-on management, with data distribution and auto repair capabilities, simplified data models and fewer tuning and administration requirements.

 4. NoSQL databases can be easily installed in cheap commodity hardware clusters as transaction and data volumes increase. This means that you can process and store more data at much less cost.


# Some NoSQL Non-Relational Database Systems

## 1. MongoDb
-  ***Free to use***: Since October 2018, MongoDB's updates have been published under the Server Side Public License (SSPL) v1, and the database is free to use.
- ***Dynamnic Schema***: As mentioned, this gives you the flexibility to change your data schema without modifying any of your existing data.
-  ***Scalability***: MongoDB is horizontally scalable, which helps reduce the workload and scale your business with ease.
- ***Manageability***: The database doesn’t require a database administrator. Since it is fairly user-friendly in this way, it can be used by both developers and administrators.
- ***Speed***: It’s high-performing for simple queries.
- ***Not Acid Compliant***: As a NoSQL database, MongoDB is not ACID compliant.
- ***Who Should Use It***: MongoDB is a good choice for businesses that have rapid growth or databases with no clear schema definitions (i.e., you have a lot of unstructured data). If you cannot define a schema for your database, if you find yourself denormalizing data schemas, or if your data requirements and schemas are constantly evolving - as is often the case with mobile apps, real-time analytics, content management systems, etc. - MongoDB can be a strong choice for you.
  
## 2. Apache Cassandra
- ***Free and Open-Source***: After Facebook made Cassandra open-source, Apache took over the project in 2010.
- ***Highly scalable***: Cassandra benefits from a "masterless design." That means all of its nodes are identical, which creates operational simplicity, making it easy to scale up to a larger database architecture.
- ***Active everywhere***: Users can write and read from all Cassandra nodes.
- ***Fast writes and reads***: Cassandra's design speeds up read and write commands tremendously via its distributed, highly-available organization, even in the case of massive projects.
- ***Support for SQL***: Even though it's not ACID compliant, Cassandra does offer some support for SQL via SQL-like DDL, DML, and SELECT statements.
- ***Who Should Use It?*** : Cassandra is most popular for use with IoT (internet of things) technology because it offers fast, real-time insights. It excels at writing time-based log activities, error logging, and sensor data. If you need fast read and write processing, Cassandra could be the right database.

## 3. Google Cloud BigTable
- ***Low latency***: According to Google, BigTable offers a consistent sub-10ms latency.
- ***Replication***: Through replication, BigTable provides higher availability, durability, and resilience when zonal failures happen. Replication also offers "high availability for live serving apps, and workload isolation for serving vs. analytics.
- ***Machine learning***: BigTable features a storage engine for use with machine learning applications.
- ***Easy to integrate***: Integrates well with open-source data analytics tools.
- ***Highly scalable***: Google BigTable can work with massive data sources in the hundreds of petabytes scale.
- ***Fully managed with Integrations***: Like MongoDB Atlas, BigTable is fully managed, which reduces workload requirements. It also integrates instantly with many platforms, which streamlines the ETL processes required to load data.
- ***Highly compatible with Google services***: As a Google product, BigTable integrates well with other services under the Google umbrella.
- ***When Should You Use It?*** : According to Google, BigQuery is great for fintech, IoT, and advertising technology as well as other use cases. For fintech, you can create a check for fraud patterns and watch real-time transaction information. You can also save and consolidate financial market data, trading activity, and more. For IoT, you can ingest and understand massive amounts of real-time time series data recorded from sensors to create dashboards and valuable analytics. For advertising, you can gather large amounts of customer behavior data to find patterns that inform your marketing efforts.
  
## 4. Apache HBase
- ***Open-source and free*** : Apache HBase is an open-source, free, NoSQL database system managed by Apache. It was modeled after Google Cloud BigTable (above), to offer BigTable-like features on top of the Hadoop Distributed File System (HDFS).
- ***Massive tables***: HBase was specifically created to manage large datasets.
- ***Scales across a cluster*** : Hbase is excellent at scaling across a cluster. Clusters relate to clustering algorithms, which are used to derive machine learning insights from data.
- ***Data management***: HBase organizes rows into "regions." The regions determine how the table will be divided across more than one node that make up a cluster. If one of the regions is too big, HBase automatically breaks it up to evenly distribute the load across more than one server.
- ***Works with both unstructured and semi-structured data*** : As a NoSQL database, HBase is ideal for storing both semi-structured and structured information.
- ***Consistency*** : HBase offers fast, consistent processing of read and write commands. After performing a write, all of the read requests on the data will produce the same response.
- ***Failover*** : HBase uses replication to offer failover, which reduces or eliminates the negative impact of a system failure on users.
- ***When Should You Use It?*** : The Apache HBase website advises to use HBase "when you need random, realtime read/write access to your big data." The database is designed to host massive tables of information that include billions of rows and millions of columns.

# Refrences

- https://www.xplenty.com/blog/the-sql-vs-nosql-difference/#:~:text=SQL%20databases%20are%20relational%2C%20NoSQL%20are%20non%2Drelational.&text=NoSQL%20databases%20have%20dynamic%20schemas,graph%20or%20wide%2Dcolumn%20stores.
- https://www.hadoop360.datasciencecentral.com/blog/advantages-and-disadvantages-of-nosql-databases-what-you-should-k
- https://www.mongodb.com/nosql-explained
